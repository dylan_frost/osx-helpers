## Instructions For Use ##

1. Copy .bash_profile and .bashrc into user's home directory
2. Replace variable exports at the top of .alias and .custom_cmds with
	appropriate values
3. New terminal windows should now support custom functionality contained
	in this repository
